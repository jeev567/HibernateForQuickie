package com.superman.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.superman.model.Laptop;
import com.superman.model.Students;

/**
 * 
 * @author jeev567
 *This class is to focus on Different types of Fetch
 */
public class App2 {
	public static void main(String[] args) {
Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Students.class).addAnnotatedClass(Laptop.class);
		
		
		//Service to remove depricated usage
		ServiceRegistry regs = new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf = con.buildSessionFactory(regs);

		Session session = sf.openSession	();
		Transaction tx = session.beginTransaction();
		
		//READ 
		Students s = (Students) session.get(Students.class,"u1154472");
		
		//When you specifically ask for list the object will fetch from database other wise it will not
		//Default is Lazy Initialization
		//If you want to make it Eager u add fetch = FetchType.Lazy Default is Left Outer Join.
		System.out.println(s.getLaptop().size());
		
		tx.commit();
		
		
	}
}
