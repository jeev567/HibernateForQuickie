package com.superman.main;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

import com.superman.config.CustomHibernateSession;
import com.superman.model.Students;


/**
 * 
 * @author jeev567
 * App6UsingSQLinHibernate: This file helps us understand that Native SQL can also be used 
 */

public class App6UsingSQLinHibernate {
	public static void main(String[] args) {
		CustomHibernateSession c = new CustomHibernateSession();
		Session s = c.getSession();
		c.startTransaction(s);
		
		//One Way
		Query q = s.createSQLQuery("select * from students").addEntity(Students.class);
		List<Students> sList = q.list();
	
		for (Students stu : sList) {
			System.out.println(stu.getName());
			System.out.println(stu.getAge());
		}
		
		//Second way
		Query q2 = s.createSQLQuery("select uid, fname from students").setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List students = q2.list();

		for (Object object : students) {
			Map m  = (Map)object;
			System.out.println(m.get("uid")+": "+m.get("fname"));
		}
		
		c.commitTransaction(s);
		c.closeSession(s);
		

	}
}
