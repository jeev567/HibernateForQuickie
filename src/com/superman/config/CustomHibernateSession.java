package com.superman.config;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.superman.model.Laptop;
import com.superman.model.Students;

public class CustomHibernateSession {

	

	public Session getSession() {
		Configuration con = new Configuration().configure("hibernate.cfg.xml")
				.addAnnotatedClass(Students.class)
				.addAnnotatedClass(Laptop.class);
		ServiceRegistry regs = new ServiceRegistryBuilder().applySettings(
				con.getProperties()).buildServiceRegistry();
		SessionFactory sf = con.buildSessionFactory(regs);
		return sf.openSession();
		
	}

	public void startTransaction(Session session) {
		session.beginTransaction();
	}

	public void commitTransaction(Session session) {
		session.getTransaction().commit();
	}

	public void closeSession(Session session) {
		session.close();
	}

}
