package com.superman.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.superman.model.Laptop;
import com.superman.model.Students;

/**
 * 
 * @author jeev567
 *
 */
public class App3Cache {
	public static void main(String[] args) {
Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Students.class).addAnnotatedClass(Laptop.class);
		
		
		//Service to remove depricated usage
		ServiceRegistry regs = new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf = con.buildSessionFactory(regs);
		///Session session = sf.openSession();
		///Transaction tx = session.beginTransaction();
		
		//First Level Cache Example 
		///Students s  = (Students)session.get(Students.class,"u1154472");
		///System.out.println(s.getName());
		// In the second fetch it will not query but get from first level cache
		///s  = (Students)session.get(Students.class,"u1154472");
		///System.out.println(s.getName());
		///session.close();
		
		//Second Level Cache
		System.out.println("Second Level Cache.....");
		//Session A
		Session sessionA = sf.openSession();
		sessionA.beginTransaction();
		Students sA  = (Students)sessionA.get(Students.class,"u1154472");
		System.out.println(sA.getName());
		sessionA.getTransaction().commit();
		sessionA.close();
		
		
		// Session B		
		Session sessionB = sf.openSession();
		sessionB.beginTransaction();
		sA = (Students)sessionB.get(Students.class,"u1154472");
		System.out.println(sA.getName());
		sessionB.getTransaction().commit();
		sessionB.close();
		
		///tx.commit();
		
		
	}
}
