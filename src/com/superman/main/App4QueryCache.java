package com.superman.main;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.superman.model.Laptop;
import com.superman.model.Students;

/**
 * 
 * @author jeev567
 * By default when you enable second level cache it is only applicable for Session.get
 * But enable across the query we add few properties in Hibernate Config File #ChangeMade
 */
public class App4QueryCache {
	public static void main(String[] args) {
Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Students.class).addAnnotatedClass(Laptop.class);
		
		
		//Service to remove depricated usage
		ServiceRegistry regs = new ServiceRegistryBuilder().applySettings(con.getProperties()).buildServiceRegistry();
		SessionFactory sf = con.buildSessionFactory(regs);
		
		//Second Level Cache
		System.out.println("Second Level Cache.....");
		//Session A
		Session sessionA = sf.openSession();
		sessionA.beginTransaction();
		Query q1 = sessionA.createQuery("from Students where uid ='u1154472'");
		q1.setCacheable(true);
		Students s1 = (Students)q1.uniqueResult();
		
		System.out.println(s1.getName());
		sessionA.getTransaction().commit();
		sessionA.close();
		
		
		// Session B		
		Session sessionB = sf.openSession();
		sessionB.beginTransaction();
		Query q2 = sessionB.createQuery("from Students where uid ='u1154472'");
		q2.setCacheable(true);
		s1 = (Students)q2.uniqueResult();
		System.out.println(s1.getName());
		sessionB.getTransaction().commit();
		sessionB.close();
		
		///tx.commit();
		
		
	}
}
