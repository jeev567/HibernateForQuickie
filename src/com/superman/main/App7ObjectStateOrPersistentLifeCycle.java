package com.superman.main;

import org.hibernate.Session;

import com.superman.config.CustomHibernateSession;
import com.superman.model.Students;

/**
 * 
 * @author jeev567
 * Some notes : /Hibernatev1/src/com/superman/note/Quick Notes
 * 
 */
public class App7ObjectStateOrPersistentLifeCycle {
	public static void main(String[] args) {
		CustomHibernateSession c= new CustomHibernateSession();
		Session s = c.getSession();
		c.startTransaction(s);
		
		//State : NEW
		Students  st1 = new Students();
		st1.setAge(25);
		st1.setUid("TestId2");
		s.save(st1);
		//State : PERSIST (The changes will be visibile in Database without save)		
		st1.setAge(26);
		
		//DETACHED
		c.commitTransaction(s);
		c.closeSession(s);
		st1.setAge(27);
		
		
		//Similary Remove
		
		
		
	}
}
