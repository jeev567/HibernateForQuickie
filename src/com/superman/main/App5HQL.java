package com.superman.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.superman.config.CustomHibernateSession;


/**
 * 
 * @author jeev567
 * This file helps us understand HQL also this file has seperated Hibernate session creation into seperate file 
 */

public class App5HQL {
	public static void main(String[] args) {
		CustomHibernateSession c = new CustomHibernateSession();
		Session s = c.getSession();
		c.startTransaction(s);
		
		//Query q = s.createQuery("from Students where uid ='u1154474'");
		Query q = s.createQuery("select uid,name from Students");
		List<Object[]> students  = (List<Object[]>)q.list();
		
		//Parameterized Query
		//	Query q = s.createQuery("select uid,name from Students s where s.uid = :hee ");
		//q.setParameter(":hee", 'u1154473a')
		for (Object[] objects : students) {
			System.out.println(objects[0]);
		}
		
		/*for (Students students : list) {
			System.out.println("Id:"+students.getUid());
			System.out.println("Name"+students.getName());
		}*/
		

		c.commitTransaction(s);
		c.closeSession(s);

	}
}
