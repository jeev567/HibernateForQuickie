package com.superman.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "Students")
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Students {

	@Column
	@Id
	private String uid;
	@Column
	private Name name;
	
	//@OneToOne
	//@OneToMany(mappedBy="student")
	@ManyToMany(mappedBy="studentList",fetch=FetchType.LAZY)
	private List<Laptop> laptop = new ArrayList<>();

	@Transient
	private String email;
	@Column
	private int age;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}
/*
	public Laptop getLaptop() {
		return laptop;
	}

	public void setLaptop(Laptop laptop) {
		this.laptop = laptop;
	}*/
	
	

	@Override
	public String toString() {
		return "Student[uid =" + uid + "and first name =" + name + " ]";
	}

	public List<Laptop> getLaptop() {
		return laptop;
	}

	public void setLaptop(List<Laptop> laptop) {
		this.laptop = laptop;
	}
}
