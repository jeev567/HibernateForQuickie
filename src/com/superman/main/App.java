package com.superman.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.superman.model.Laptop;
import com.superman.model.Name;
import com.superman.model.Students;

/**
 * 
 * @author jeev567
 * 
 */
public class App {
	public static void main(String[] args) {
		
		Laptop l = new Laptop();
		l.setId(1);
		l.setName("Dell");
		
		
		/*Laptop l = new Laptop();
		l.setId(1);
		l.setName("Dell");
		*/
		Name n= new Name();
		n.setFname("Jeevan");
		n.setMname("Thomas");
		n.setLname("Abraham");
		
		Students s = new Students();
		s.setAge(30);
		s.setEmail("jeev567@gmail.com");
		s.setName(n);
		s.setUid("u115447A");
		//s.setLaptop(l);
		s.getLaptop().add(l);
		l.getStudentList().add(s);

		Configuration con = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Students.class).addAnnotatedClass(Laptop.class);
		
		
		//Service to remove depricated usage
		ServiceRegistry regs = new ServiceRegistryBuilder().applySettings(
				con.getProperties()).buildServiceRegistry();
		SessionFactory sf = con.buildSessionFactory(regs);

		Session session = sf.openSession	();
		Transaction tx = session.beginTransaction();
		session.save(l);
		session.save(s);
		
		//READ 
		//s = (Students) session.get(Students.class,"u115447A");
		tx.commit();
		
		System.out.println(s);

	}
}
